const { executeSQL, executeInsertSQL } = require('./tools/database.js')
const { log } = require('./tools/log.js')
const { computeCron, cronTimeIntersect } = require('./model/cron.js')

let activeTasks = []
let isComputing = false
let isComputingRedo = false
let _lastDateOffset = 0
let computing = Promise.resolve()

function computeNext(dateOffset = 0) {
  _lastDateOffset = dateOffset
  if (isComputing) {
    isComputingRedo = true
    return computing
  }
  isComputing = true
  return (computing = doComputeNext(_lastDateOffset)
    .then(() => (isComputing = false))
    .catch(() => (isComputing = false))
    .then(() => {
      if (isComputingRedo) {
        isComputingRedo = false
        return computeNext(_lastDateOffset)
      } else Promise.resolve()
    }))
}

async function doComputeNext(dateOffset = 0) {
  log('ScheduleNext:Begin')

  if (typeof dateOffset !== 'number') dateOffset = 0

  for (const activeTask of activeTasks) {
    clearTimeout(activeTask.timeout)
    if (activeTask.program) await activeTask.program.cancel()
  }
  activeTasks = []

  const now = new Date()
  const nowMonth = now.getMonth()
  const nowDate = now.getDate() + dateOffset

  await executeSQL`DELETE FROM computed_crons`

  const crons = await executeSQL`
    SELECT *
    FROM crons
  `

  for (const cron of crons) {
    for (const { cron_id, time } of computeCron(cron)) {
      if (time.getMonth() === nowMonth && time.getDate() === nowDate) {
        await executeInsertSQL('computed_crons', {
          cron_id,
          time: time,
        })
      }
    }
  }
  const computeCrons = await executeSQL`
    SELECT cron_id, time
    FROM computed_crons
    ORDER BY time ASC
  `

  for (const nextSchedule of computeCrons) {
    const nextScheduleUnix = nextSchedule.time.getTime()
    const schedules = [
      ...(await executeSQL`
        SELECT
          schedule_include_crons.schedule_id AS schedule_id,
          schedules.name AS schedule_name
        FROM schedule_include_crons
        JOIN schedules ON schedules.id = schedule_include_crons.schedule_id
        JOIN schedule_tags ON schedule_tags.schedule_id = schedule_include_crons.schedule_id
        JOIN tags ON tags.name = schedule_tags.tag_name
        WHERE schedule_include_crons.cron_id = ${
          nextSchedule.cron_id
        } AND tags.is_active = 1
      `),
      ...(await executeSQL`
        SELECT
          schedule_include_times.schedule_id AS schedule_id,
          schedule_include_times.time_id AS time_id,
          schedules.name AS schedule_name
        FROM schedule_include_times
        JOIN time_crons ON time_crons.time_id = schedule_include_times.time_id
        JOIN schedules ON schedules.id = schedule_include_times.schedule_id
        JOIN schedule_tags ON schedule_tags.schedule_id = schedule_include_times.schedule_id
        JOIN tags ON tags.name = schedule_tags.tag_name
        WHERE time_crons.cron_id = ${
          nextSchedule.cron_id
        } AND tags.is_active = 1
      `),
    ]
    for (const schedule of schedules) {
      let isSkip = false
      const intersectCrons = await executeSQL`
        SELECT
          schedule_id,
          months,
          dates,
          days,
          hours,
          minutes,
          seconds
        FROM schedule_intersect_crons
        WHERE schedule_id = ${schedule.schedule_id}
      `

      for (const intersectCron of intersectCrons) {
        if (!cronTimeIntersect(nextSchedule.time, intersectCron)) {
          isSkip = true
        }
      }

      if (isSkip) continue

      const tasks = await executeSQL`
        SELECT
          schedule_tasks.name AS task_name,
          schedule_tasks.payload AS payload,
          schedule_tasks.timing AS timing,
          schedule_tasks.offset AS offset
        FROM schedule_tasks
        WHERE schedule_tasks.schedule_id = ${schedule.schedule_id}
      `

      for (const task of tasks) {
        const Program = require(`./tasks/${task.task_name}.js`)
        const program = new Program(task.payload, 0)
        const duration = await program.duration()
        const nowUnix = new Date().getTime()
        const scheduleTime =
          nextScheduleUnix -
          nowUnix +
          task.offset -
          (task.timing === 'before' ? duration : 0)
        program.timeLeft = scheduleTime
        let resolve = null
        const promise = new Promise((_resolve) => (resolve = _resolve))
        const isFuture = nextScheduleUnix > nowUnix
        const timeout = isFuture
          ? setTimeout(() => {
              log('Task:Begin:', task.task_name, task.payload)
              program
                .main()
                .then(() => log('Task:Done:', task.task_name))
                .catch((error) => log('Task:Error:', task.task_name, error))
                .then(resolve)
            }, scheduleTime)
          : setTimeout(() => resolve())
        activeTasks.push({
          isFuture,
          program,
          promise,
          timeout,
          task: {
            duration,
            ...task,
            ...nextSchedule,
            ...schedule,
          },
        })
      }
    }
  }

  for (const activeTask of activeTasks) {
    log('ScheduledTask:', activeTask.task.time, activeTask.task.task_name)
  }

  if (
    activeTasks.length === 0 ||
    !activeTasks.find(({ isFuture }) => isFuture)
  ) {
    const nextDateOffset = dateOffset + 1
    if (nextDateOffset === 10) {
      const nextDate = new Date(
        now.getFullYear(),
        nowMonth,
        nowDate - 3,
        1,
        0,
        0,
        0
      )
      log('ScheduleNext:Pending', 'No schedule found until', nextDate)
      activeTasks.push({
        isFuture: true,
        program: null,
        promise: Promise.resolve(),
        timeout: setTimeout(computeNext, nextDate - now.getTime()),
        task: {
          task_name: 'ScheduleNextRetry',
          time: nextDate,
        },
      })
    } else {
      log('ScheduleNext:Retry', nextDateOffset)
      computeNext(nextDateOffset)
    }
  } else {
    Promise.all(activeTasks.map(({ promise }) => promise)).then(() =>
      computeNext()
    )
    log('ScheduleNext:Done')
  }
}

function getActiveTasks() {
  return activeTasks
}

module.exports = {
  computeNext,
  getActiveTasks,
}
