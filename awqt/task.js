class Task {
  /**
   * @param {string} payload 
   * @param {number} timeLeft 
   */
  constructor(payload, timeLeft) {
    this.payload = payload
    this.timeLeft = timeLeft
  }
}

module.exports = Task
