const Task = require('../task.js')
const AudioPlay = require('./AudioPlay.js')
const SpeakerOn = require('./SpeakerOn.js')
const SpeakerOff = require('./SpeakerOff.js')

class AudioPlayUsingSpeaker extends Task {
  constructor(...props) {
    super(...props)
    this.speakerOn = new SpeakerOn()
    this.speakerOff = new SpeakerOff()
    this.audioPlay = new AudioPlay(this.payload, 0)
  }
  main() {
    const bagining = new Date().getTime()
    return this.speakerOn.main()
      .then(() => {
        this.isSpeakerOn = true
        this.audioPlay.timeLeft = this.timeLeft + new Date().getTime() - bagining
        return this.audioPlay.main()
      })
      .then(() => this.speakerOff.main())
      .then(() => this.isSpeakerOn = false)
  }
  duration() {
    return Promise.all([
      this.speakerOn.duration(),
      this.audioPlay.duration(),
      this.speakerOff.duration(),
    ]).then((durations) => {
      return durations.reduce((sum, duration) => sum + duration, 0)
    })
  }
  cancel() {
    return this.audioPlay ? this.audioPlay.cancel() : Promise.resolve()
  }
}

module.exports = AudioPlayUsingSpeaker
