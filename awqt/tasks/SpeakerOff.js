const Task = require('../task.js')
const { SPEAKER_PINS } = require('../config.js')
const PinOff = require('./PinOff.js')

class SpeakerOff extends Task {
  main() {
    return Promise.all(
      SPEAKER_PINS.map((PIN) => new PinOff(PIN).main())
    )
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}

module.exports = SpeakerOff
