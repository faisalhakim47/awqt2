const Task = require('../task.js')
const { media } = require('../tools/mediaplayer.js')

module.exports = class AudioStop extends Task {
  main() {
    return media.stop()
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}
