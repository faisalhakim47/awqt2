const Task = require('../task.js')
const { SPEAKER_PINS } = require('../config.js')
const PinOn = require('./PinOn.js')

class SpeakerOn extends Task {
  main() {
    return Promise.all([
      Promise.all(SPEAKER_PINS.map((PIN) => new PinOn(PIN).main())),
      new Promise((resolve) => setTimeout(resolve, 3000)),
    ])
  }
  duration() {
    return Promise.resolve(3000)
  }
  cancel() {
    return Promise.resolve()
  }
}

module.exports = SpeakerOn
