const Task = require('../task.js')
const { on } =require('../tools/rpi_pin.js')

class PinOn extends Task {
  main() {
    return on(parseInt(this.payload, 10))
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}

module.exports = PinOn
