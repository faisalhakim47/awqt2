const Task = require('../task.js')
const { media } = require('../tools/mediaplayer.js')

module.exports = class AudioResume extends Task {
  main() {
    media.resume()
    return Promise.resolve()
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}
