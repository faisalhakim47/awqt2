const Task = require('../task.js')
const { executeSQL } = require('../tools/database.js')
const { media } = require('../tools/mediaplayer.js')

class MediaPlay extends Task {
  main() {
    const { md5s } = JSON.parse(this.payload)
    media.play(md5s, this.timeLeft * -1)
    return Promise.resolve()
  }
  duration() {
    const { md5s } = JSON.parse(this.payload)
    return Promise.all(
      md5s.map((md5) => executeSQL`
        SELECT duration
        FROM audios
        WHERE md5 = '${md5}'
      `)
    ).then((audios) => {
      return audios
        .map(audio => audio[0].duration)
        .reduce((total, duration) => total + duration, 0)
    })
  }
  cancel() {
    return this.player ? this.player.stop() : Promise.resolve()
  }
}

module.exports = MediaPlay
