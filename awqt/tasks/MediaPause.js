const Task = require('../task.js')
const { media } = require('../tools/mediaplayer.js')

class MediaPause extends Task {
  main() {
    return media.pause()
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}

module.exports = MediaPause
