const Task = require('../task.js')
const { media } = require('../tools/mediaplayer.js')

module.exports = class AudioStop extends Task {
  main(payload) {
    const { seek } = JSON.parse(this.payload)
    media.seek(parseInt(seek, 10))
    return Promise.resolve(0)
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}
