const Task = require('../task.js')
const { off } =require('../tools/rpi_pin.js')

class PinOff extends Task {
  main() {
    return off(parseInt(this.payload, 10))
  }
  duration() {
    return Promise.resolve(0)
  }
  cancel() {
    return Promise.resolve()
  }
}

module.exports = PinOff
