const fs = require('fs')
const path = require('path')

let config = {
  "DEVELOPMENT_MODE": true,
  "DB_USER": "root",
  "DB_PASS": "root",
  "DB_HOST": "127.0.0.1",
  "DB_PORT": 3306,
  "DB_NAME": "awqt",
  "APP_URL": "http://localhost:8080/",
  "AUDIO_PATH": "/awqt/data/audios/",
  "SPEAKER_PINS": [
    18,
    17,
    27,
    22
  ]
}

config = JSON.parse(
  fs.readFileSync(
    path.join(__dirname, '../../config.json'),
    { encoding: 'UTF8' }
  ) || '{}'
)

config.AUDIO_DIR = path.join(__dirname, '../..', config.AUDIO_PATH)

module.exports = config
