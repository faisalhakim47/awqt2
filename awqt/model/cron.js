/**
 * @param {string} cronPart
 */
function partSplit(cronPart) {
  return cronPart.split(',').map((str) => parseInt(str, 10))
}

function cronTimeIntersect(datetime, cronIntersect) {
  const date = new Date(datetime)
  const cronTime = [
    ['months', (date.getMonth() + 1).toString()],
    ['dates', date.getDate().toString()],
    ['days', (date.getDay() + 1).toString()],
    ['hours', date.getHours().toString()],
    ['minutes', date.getMinutes().toString()],
    ['seconds', date.getSeconds().toString()],
  ]

  function partIntersect(time, cron) {
    if (cron === '*') return true
    if (partSplit(cron).indexOf(parseInt(time, 10)) !== -1) return true
    return false
  }

  for (const [type, timePart] of cronTime) {
    if (!partIntersect(timePart, cronIntersect[type])) {
      return false
    }
  }

  return true
}

function computeCron(cron) {
  /**
   * @param {string} part
   * @param {number[]} fallback
   */
  const parseTimePart = (part, fallback) => {
    return part === '*'
      ? fallback
      : part.split(',').map((time) => parseInt(time, 10))
  }

  /**
   * @param {number} start
   * @param {number} end
   */
  const range = (start, end) => {
    return Array.from({ length: end - start + 1 }).map(
      (_, index) => start + index
    )
  }

  /**
   * @param {number} year
   * @param {number} month
   */
  const daysInMonth = (year, month) => {
    return new Date(year, month + 1, 0).getDate()
  }

  const year = new Date().getFullYear()
  const months = parseTimePart(cron.months, range(1, 12))
  const days = parseTimePart(cron.days, range(1, 7))
  const hours = parseTimePart(cron.hours, range(0, 23))
  const minutes = parseTimePart(cron.minutes, range(0, 59))
  const seconds = parseTimePart(cron.seconds, range(0, 59))

  const computedCrons = []

  for (const month of months) {
    let dates = parseTimePart(cron.dates, range(1, daysInMonth(year, month)))

    if (cron.days !== '*')
      dates = dates.filter((date) => {
        const dayOfWeek = new Date(year, month - 1, date, 1).getDay() + 1
        return days.indexOf(dayOfWeek) !== -1
      })

    for (const date of dates)
      for (const hour of hours)
        for (const minute of minutes)
          for (const second of seconds)
            computedCrons.push({
              cron_id: cron.id,
              time: new Date(year, month - 1, date, hour, minute, second),
            })
  }

  return computedCrons
}

module.exports = {
  computeCron,
  cronTimeIntersect,
}
