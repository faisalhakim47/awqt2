const child_process = require('child_process')
const Koa = require('koa')
const { computeNext, getActiveTasks } = require('./schedule.js')
const { log } = require('./tools/log.js')
const { setAccessPoint } = require('./tools/wifi.js')
const { media } = require('./tools/mediaplayer.js')

const app = new Koa()

app.use(async (ctx) => {
  const pathParts = ctx.path
    .split('/')
    .filter((part) => part)

  if (ctx.path === '/compute_next') {
    await computeNext()
    ctx.body = { ok: true }
  }

  else if (ctx.path === '/active_tasks') {
    ctx.body = getActiveTasks()
      .map(({ task }) => task)
      .filter((task) => task)
  }

  else if (pathParts[0] === 'mediaplayer_state') {
    ctx.body = media.player.status
  }

  else if (pathParts[0] === 'volume') {
    if (pathParts[1]) await new Promise((resolve, reject) => {
      child_process.exec(`amixer set PCM ${pathParts[1]}% | awk -F'[][]' '/dB/ { print $2 }'`, (error, stdout) => {
        if (error) reject(error)
        else resolve(parseInt(stdout.split('%')[0] || '0', 10))
      })
    }).then((volume) => {
      ctx.body = { volume }
    }).catch((error) => {
      ctx.status = 500
      ctx.body = error
    })
    else await new Promise((resolve, reject) => {
      child_process.exec(`amixer get PCM | awk -F'[][]' '/dB/ { print $2 }'`, (error, stdout) => {
        if (error) reject(error)
        else resolve(parseInt(stdout.split('%')[0] || '0', 10))
      })
    }).then((volume) => {
      ctx.body = { volume }
    }).catch((error) => {
      ctx.status = 500
      ctx.body = error
    })
  }

  else if (pathParts[0] === 'set_access_point') {
    const [ssid, passphrase] = pathParts[1].split(':')
    setAccessPoint({ ssid, passphrase })
    setTimeout(() => {
      // I dont know why but it has to be restared twice
      child_process.execSync('sudo systemctl restart hostapd dhcpcd')
      child_process.execSync('sudo systemctl restart hostapd dhcpcd')
    }, 2000)
    ctx.body = { ok: true }
  }

  else if (pathParts[0] === 'execute') {
    const Program = require(`./tasks/${pathParts[1]}.js`)
    const program = new Program(pathParts[2], 0)
    log('Task:Begin:', pathParts[1], pathParts[2])
    await program.main()
      .then((body) => {
        ctx.body = body
        log('Task:Done:', pathParts[1])
      })
      .catch((error) => {
        ctx.status = 500
        ctx.body = error
        log('Task:Error:', pathParts[1], error)
      })
  }

  else {
    ctx.status = 404
    ctx.body = JSON.stringify({
      headers: ctx.headers,
      idempotent: ctx.idempotent,
      type: ctx.type,
      URL: ctx.URL,
      url: ctx.url,
      ip: ctx.ip,
      ips: ctx.ips,
      query: ctx.query,
      path: ctx.path,
      request: ctx.request,
    }, null, 2)
  }
})

app.listen(3131, '0.0.0.0')

computeNext()
