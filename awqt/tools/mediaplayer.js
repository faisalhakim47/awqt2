const { AudioPlayer } =  require('./audio_player.js')

const media = new AudioPlayer()

module.exports = {
  media,
}
