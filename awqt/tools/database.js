const mysql = require('mysql')
const { DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_NAME } = require('../config.js')

const pool = mysql.createPool({
  host: DB_HOST,
  port: DB_PORT,
  user: DB_USER,
  password: DB_PASS,
  database: DB_NAME,
  connectionLimit: 10,
})

/**
 * @param {TemplateStringsArray} strings 
 * @param {any[]} expressions 
 */
async function executeSQL(strings, ...expressions) {
  const sql = strings
    .map((string, index) => {
      let expression = expressions[index]
      if (expression instanceof Date) {
        expression = mysql.escape(expression)
      }
      return string + (expression || '')
    })
    .join('')
  return await new Promise((resolve, reject) => {
    pool.query(sql, (error, results) => {
      if (error) reject(error)
      else resolve(results)
    })
  })
}

/**
 * @param {string} tableName 
 * @param {object} data 
 */
async function executeInsertSQL(tableName, data) {
  const escapedData = Object.keys(data)
    .map((key) => {
      return {
        key: mysql.escapeId(key),
        value: mysql.escape(data[key]),
      }
    })
  const keys = escapedData
    .map((data) => data.key)
    .join(',')
  const values = escapedData
    .map((data) => data.value)
    .join(',')
  return await executeSQL`
    INSERT INTO ${tableName} (${keys})
    VALUES (${values})
  `
}

module.exports = {
  executeSQL,
  executeInsertSQL,
}
