const child_process = require('child_process')
const fs = require('fs')

function setAccessPoint({ ssid = '', passphrase = '' } = {}) {
  const properties = fs.readFileSync('/etc/hostapd/hostapd.conf', { encoding: 'utf8' })
    .split('\n')
    .map((property) => property.split('='))
    .filter(([key, value]) => value && key)
    .map(([key, value]) => {
      if (key === 'ssid') value = ssid
      if (key === 'wpa_passphrase') value = passphrase
      return `${key}=${value}`
    })

  child_process.execSync('sudo rm /etc/hostapd/hostapd.conf')
  child_process.execSync('sudo touch /etc/hostapd/hostapd.conf')

  for (const property of properties) {
    child_process.execSync(`sudo sh -c "echo '${property}' >> /etc/hostapd/hostapd.conf"`)
  }
}

module.exports = {
  setAccessPoint,
}
