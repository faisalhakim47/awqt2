const isRpi = require('detect-rpi')
const request = require('request-promise-native')

module.exports = isRpi()
  ? {
    on(pin) {
      return request(`http://localhost:9000/pin/${pin}/on`)
    },

    off(pin) {
      return request(`http://localhost:9000/pin/${pin}/off`)
    },

    toggle(pin) {
      return request(`http://localhost:9000/pin/${pin}/toggle`)
    },
  } : {
    on(pin) {
      console.info('PIN_ON', pin)
      return Promise.resolve(pin)
    },

    off(pin) {
      console.info('PIN_OFF', pin)
      return Promise.resolve(pin)
    },

    toggle(pin) {
      console.info('PIN_TOGGLE', pin)
      return Promise.resolve(pin)
    },
  }
