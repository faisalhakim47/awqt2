const child_process = require('child_process')
const MPlayer = require('mplayer')
const { AUDIO_DIR } = require('../config.js')
const { executeSQL } = require('./database.js')

child_process.execSync('mkdir -p ' + AUDIO_DIR)

class AudioPlayer {
  constructor() {
    this.player = new MPlayer()
    this.playlist = []
    this.index = 0
    this.skip = 0
    this.isForceStop = false
    this.isPlaying = false
  }

  /**
   * @param {string[]} md5s 
   * @param {number} skip 
   */
  async play(md5s, skip = 0) {
    this.playlist = md5s
    this.skip = skip
    return await Promise.all(md5s.map((md5) => executeSQL`
      SELECT duration
      FROM audios
      WHERE md5 = '${md5}'
    `)).then((audios) => new Promise((resolve) => {
        for (const duration of audios.map(audio => audio[0].duration)) {
          if (this.skip < duration) break
          this.skip -= duration
          this.index++
        }
        const playNext = () => {
          const md5 = this.playlist[this.index]
          if (md5 && !this.isForceStop) {
            this.player.openFile(AUDIO_DIR + md5)
            this.player.seek(this.skip / 1000)
            this.player.play()
            this.isPlaying = true
            this.index++
          } else {
            this.player.off('stop', playNext)
            this.isPlaying = false
            this.index = 0
            resolve()
          }
          this.skip = 0
          this.currentTime = 0
          this.isForceStop = false
        }
        playNext()
        this.player.on('stop', playNext)
      })).catch((error) => {
        console.warn(error)
        throw error
      })
  }

  stop() {
    return new Promise((resolve) => {
      if (!this.player.status.filename) return resolve()
      const onStop = () => {
        this.player.off('stop', onStop)
        this.index = 0
        resolve()
      }
      this.player.on('stop', onStop)
      this.isForceStop = true
      this.player.stop()
    })
  }

  pause() {
    return new Promise((resolve) => {
      if (!this.player.status.playing) return resolve()
      const onPause = () => {
        this.player.off('pause', onPause)
        resolve()
      }
      this.player.on('pause', onPause)
      this.player.pause()
    })
  }

  resume() {
    return new Promise((resolve) => {
      if (this.player.status.playing) return resolve()
      const onPlay = () => {
        this.player.off('play', onPlay)
        resolve()
      }
      this.player.on('play', onPlay)
      this.player.play()
    })
  }

  seek(seek = 0) {
    this.player.seek(seek / 1000)
    this.player.play()
  }
}

module.exports = {
  AudioPlayer,
}
