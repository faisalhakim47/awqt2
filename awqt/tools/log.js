const child_process = require('child_process')
const fs = require('fs')

const logFile = `${__dirname}/../../log.txt`
child_process.execSync(`touch ${logFile}`)
const logStream = fs.createWriteStream(logFile, { flags: 'a' })

function indoDate(date) {
  date = new Date(date)
  date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
  date = date.toISOString().slice(0, -5)
  return date
}

function log(...lines) {
  lines = lines.map((line) => {
    if (line == undefined) line = '' + line
    if (line instanceof Date) line = indoDate(line)
    return line
  })
  console.log(...lines)
  logStream.write('\n' + indoDate(new Date) + ' ')
  lines.forEach((line) => {
    if (line == undefined) line = '' + line
    if (line instanceof Date) line = indoDate(line)
    if (typeof line === 'object') line = JSON.stringify(line, null, 2)
    logStream.write(line.toString())
    logStream.write(' ')
  })
  return lines
}

function readLogFile({ skip = 0, limit = 5120 }) {
  return new Promise((resolve, reject) => {
    const fileSize = fs.statSync(logFile).size - skip
    const start = limit === 0 ? 0 : fileSize - limit
    const logStream = fs.createReadStream(logFile, {
      encoding: 'utf8',
      start: start < 0 ? 0 : start,
      end: fileSize,
    })
    let data = ''
    logStream.on('data', (partial) => data += partial)
    logStream.on('end', () => resolve(data))
    logStream.on('error', (error) => reject(error))
  })
}

function clearLogFile() {
  return new Promise((resolve, reject) => {
    fs.writeFile(logFile, '', { flag: 'w', encoding: 'utf8' }, (err) => {
      if (err) return reject(err)
      else resolve({ ok: true })
    })
  })
}

module.exports = {
  log,
  readLogFile,
  clearLogFile
}
