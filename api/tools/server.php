<?php

$__is_using_json = true;

function send_json($status, $object)
{
  global $__is_using_json;
  if (!$__is_using_json) {
    if ($status >= 400) {
      throw $object;
    }
    return $object;
  }
  http_response_code($status);
  http_response_code($status); // Yes, I have to execute it twice in order to make it work.
  header("Content-Type: application/json");
  echo json_encode($object);
  exit();
}

function dont_send_json($func)
{
  global $__is_using_json;
  $__is_using_json = false;
  try {
    $result = $func();
    $__is_using_json = true;
    return $result;
  } catch (Exception $error) {
    $__is_using_json = true;
    throw $error;
  }
}

/**
 * Append history log file.
 */
function write_log($line)
{
  // if (is_array($line)) $line = json_encode($line);
  // if ($line === "") return;
  // $time = date("Y-m-d H:i:s");
  // file_put_contents(__DIR__."/../../logs.txt", "{$time} - {$line}\n", FILE_APPEND);
}

function get_querystring($name, $default = "")
{
  return isset($_GET[$name]) ? $_GET[$name] : $default;
}

function require_querystring($name)
{
  $value = $_GET[$name];
  if (!isset($value)) {
    send_json(400, [
      "msg" => "require '{$name}' query",
    ]);
  }
  return $value;
}

function get_query($queries, $sql_query = "WHERE ")
{
  $prepare = [];
  foreach ($queries as $name => $query) {
    $value = $query["required"] ? require_querystring($name) : get_querystring($name);
    if ($value !== "") {
      if (isset($query["map"])) {
        $value = $query["map"]($value);
      }
      $param_type = isset($query["param_type"]) ? $query["param_type"] : PDO::PARAM_STR;
      array_push($prepare, [
        "name" => $name,
        "sql_query" => $query["sql_query"],
        "value" => [$value, $param_type],
      ]);
    }
  }
  if (count($prepare) > 0) {
    $sql_query .= implode(" AND ", array_map(function ($query) {
      return $query["sql_query"];
    }, $prepare));
    $params = [];
    foreach ($prepare as $query) {
      $params[":" . $query["name"]] = $query["value"];
    }
    return [
      "sql_query" => $sql_query,
      "params" => $params,
    ];
  } else {
    return [
      "sql_query" => "",
      "params" => [],
    ];
  }
}

function get_json_data()
{
  $json = file_get_contents('php://input');
  return json_decode($json, true);
}

function require_json_data()
{
  $json = get_json_data();
  if ($json === null) {
    send_json(400, [
      "msg" => "JSON data required.",
    ]);
  }
  return $json;
}
