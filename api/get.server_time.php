<?php

require_once __DIR__ . "/app.php";

$now = new DateTime(null, new DateTimeZone('Asia/Jakarta'));

send_json(200, [
  "time" => $now->format("Y-m-d\TH:i:sO")
]);
