<?php

require_once __DIR__ . "/app.php";

$time_id = require_querystring("time_id");

$crons = execute_sql("
  SELECT
    crons.id AS id,
    crons.months AS months,
    crons.days AS days,
    crons.dates AS dates,
    crons.hours AS hours,
    crons.minutes AS minutes,
    crons.seconds AS seconds
  FROM crons
  JOIN time_crons ON time_crons.cron_id = crons.id
  WHERE time_crons.time_id = :time_id
", [
  ":time_id" => [$time_id, PDO::PARAM_INT],
])->fetchAll();

send_json(200, $crons);
