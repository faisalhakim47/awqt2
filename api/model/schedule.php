<?php

require_once __DIR__ . "/../app.php";
require_once __DIR__ . "/tag.php";

function create_schedule_pack($schedule)
{
  return use_sql_transaction(function () use ($schedule) {
    $schedule_id = execute_insert_sql("schedules", [
      "name" => [$schedule["name"], PDO::PARAM_STR],
    ]);

    // ----- TAGS -----
    $tag_ids = array_map(function ($tag) use ($schedule_id) {
      ensure_tag($tag);
      return execute_insert_sql("schedule_tags", [
        "schedule_id" => [$schedule_id, PDO::PARAM_INT],
        "tag_name" => [$tag, PDO::PARAM_STR],
      ]);
    }, $schedule["tags"] ?: []);

    // ----- TASK -----
    $schedule_task_ids = array_map(function ($task) use ($schedule_id) {
      return execute_insert_sql("schedule_tasks", [
        "schedule_id" => [$schedule_id, PDO::PARAM_STR],
        "name" => [$task["name"], PDO::PARAM_STR],
        "payload" => [$task["payload"], PDO::PARAM_STR],
        "timing" => [$task["timing"], PDO::PARAM_INT],
      ]);
    }, $schedule["tasks"] ?: []);

    // ----- INCLUDE TIMES -----
    $include_time_ids = array_map(function ($include_time) use ($schedule_id) {
      return execute_insert_sql("schedule_include_times", [
        "schedule_id" => [$schedule_id, PDO::PARAM_INT],
        "time_id" => [$include_time["time_id"], PDO::PARAM_INT],
      ]);
    }, $schedule["include_times"] ?: []);

    // ----- INTERSECT CRONS -----
    $intersect_cron_ids = array_map(function ($intersect_cron) use ($schedule_id) {
      $intersect_cron += [
        "schedule_id" => [$schedule_id, PDO::PARAM_INT],
      ];
      return execute_insert_sql("schedule_intersect_crons", $intersect_cron);
    }, $schedule["intersect_crons"] ?: []);

    // ----- EXCLUDE CRONS -----
    $exclude_cron_ids = array_map(function ($exclude_cron) use ($schedule_id) {
      $exclude_cron += [
        "schedule_id" => [$schedule_id, PDO::PARAM_INT],
      ];
      return execute_insert_sql("schedule_exclude_crons", $exclude_cron);
    }, $schedule["exclude_crons"] ?: []);

    return [
      "schedule_id" => $schedule_id,
      "tag_ids" => $tag_ids,
      "schedule_task_ids" => $schedule_task_ids,
      "schedule_include_time_ids" => $include_time_ids,
      "schedule_intersect_cron_ids" => $intersect_cron_ids,
      "schedule_exclude_cron_ids" => $exclude_cron_ids,
    ];
  });
}
