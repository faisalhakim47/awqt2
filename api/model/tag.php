<?php

require_once __DIR__ . "/../app.php";

function is_tag_exist($tag_name)
{
  $result = execute_sql("
    SELECT EXISTS(SELECT 1 FROM tags WHERE name = :tag_name) AS is_exist;
  ", [
    ":tag_name" => [$tag_name, PDO::PARAM_STR],
  ])->fetch();
  return $result["is_exist"] === 1;
}

function ensure_tag($tag_name) {
  if (!is_tag_exist($tag_name)) {
    execute_insert_sql("tags", [
      "name" => [$tag_name, PDO::PARAM_STR],
    ]);
  }
}
