<?php

function compute_next()
{
  return json_decode(file_get_contents("http://127.0.0.1:3131/compute_next"), true);
}

function active_tasks()
{
  return json_decode(file_get_contents("http://127.0.0.1:3131/active_tasks"), true);
}

function execute_task($task_name, $payload = null)
{
  return json_decode(file_get_contents("http://127.0.0.1:3131/execute/{$task_name}/{$payload}"), true);
}
