<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "id" => [
    "sql_query" => "id = :id",
    "param_type" => PDO::PARAM_INT,
  ],
  "name" => [
    "sql_query" => "name = :name",
    "param_type" => PDO::PARAM_STR,
  ],
]);

$result = execute_sql("
  SELECT id, name
  FROM times
  {$prepare["sql_query"]}
  LIMIT 1
", $prepare["params"])->fetch();

send_json(200, $result);
