<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/tag.php";

$tag = require_json_data();

ensure_tag($tag["name"]);

send_json(200, $tag);
