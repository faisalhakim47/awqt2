<?php

require_once __DIR__ . "/app.php";

$schedule_task = require_json_data();

$result = execute_update_sql("schedule_tasks", [
  "name" => [$schedule_task["name"], PDO::PARAM_STR],
  "payload" => [$schedule_task["payload"], PDO::PARAM_STR],
  "timing" => [$schedule_task["timing"], PDO::PARAM_STR],
  "offset" => [$schedule_task["offset"], PDO::PARAM_INT],
], [
  "id" => [$schedule_task["id"], PDO::PARAM_INT],
]);

send_json(200, $result);
