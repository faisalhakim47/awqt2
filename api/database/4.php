<?php

require_once __DIR__ . "/../app.php";

alter_structure(function () {

  execute_sql("
    ALTER TABLE playlists
    ADD audio_md5s TEXT
  ")->fetchAll();

  $playlists = execute_sql("SELECT id FROM playlists")->fetchAll();

  foreach ($playlists as $playlist) {
    $playlist_audios = execute_sql("
      SELECT audio_md5
      FROM playlist_audios
      WHERE playlist_id = :playlist_id
      ORDER BY position
    ", [
      ":playlist_id" => [$playlist["id"], PDO::PARAM_INT],
    ])->fetchAll();
    execute_update_sql("playlists", [
      "audio_md5s" => [
        json_encode(array_map(function ($playlist_audio) {
          return $playlist_audio["audio_md5"];
        }, $playlist_audios)),
        PDO::PARAM_STR,
      ],
    ], [
      "playlist_id" => [$playlist["id"], PDO::PARAM_INT],
    ]);
  }

  execute_sql("DROP TABLE playlist_audios");
});
