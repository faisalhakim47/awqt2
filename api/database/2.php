<?php

require_once __DIR__ . "/../app.php";

alter_structure(function () {

  create_table("schedule_tasks", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    name VARCHAR(191) NOT NULL,
    timing VARCHAR(191) NOT NULL DEFAULT 'after',
    offset INT NOT NULL DEFAULT 0,
    payload TEXT,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id)
  ");

  $schedules = execute_sql("
    SELECT *
    FROM schedules
  ")->fetchAll();

  foreach ($schedules as $schedule) {
    $schedule_audios = execute_sql("
      SELECT *
      FROM schedule_audios
      WHERE schedule_audios.schedule_id = :schedule_id
    ", [
      ":schedule_id" => [$schedule["id"], PDO::PARAM_INT],
    ])->fetchAll();
    $schedule_task = [
      "schedule_id" => $schedule["id"],
      "name" => "AudioPlayUsingSpeaker",
      "payload" => json_encode([
        "md5s" => array_map(function ($schedule_audio) {
          return $schedule_audio["audio_md5"];
        }, $schedule_audios),
      ]),
      "timing" => "before",
      "offset" => 0,
    ];
    execute_insert_sql("schedule_tasks", $schedule_task);
  }

  execute_sql("DROP TABLE schedule_audios");
});
