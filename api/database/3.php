<?php

require_once __DIR__ . "/../app.php";

alter_structure(function () {

  create_table("schedule_include_crons", "
    schedule_id INT UNSIGNED NOT NULL,
    cron_id INT UNSIGNED NOT NULL,
    PRIMARY KEY(schedule_id, cron_id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id),
    FOREIGN KEY(cron_id) REFERENCES crons(id)
  ");

  $schedule_include_times = execute_sql("
    SELECT schedule_id, cron_id
    FROM schedule_include_times
    WHERE cron_id IS NOT NULL
  ");

  foreach ($schedule_include_times as $schedule_include_time) {
    execute_insert_sql("schedule_include_crons", $schedule_include_time);
  }

  execute_sql("ALTER TABLE schedule_include_times DROP FOREIGN KEY schedule_include_times_ibfk_2")->fetch();
  execute_sql("ALTER TABLE schedule_include_times DROP COLUMN cron_id")->fetch();

  execute_sql("ALTER TABLE schedule_include_times DROP FOREIGN KEY schedule_include_times_ibfk_3")->fetch();
  execute_sql("ALTER TABLE schedule_include_times DROP id")->fetch();
  execute_sql("ALTER TABLE schedule_include_times ADD PRIMARY KEY(schedule_id, time_id)")->fetch();
});
