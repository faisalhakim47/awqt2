<?php

require_once __DIR__ . "/../app.php";

alter_structure(function () {

  create_table("accounts", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    username VARCHAR(191) UNIQUE,
    email VARCHAR(191) UNIQUE,
    phone_number VARCHAR(191) UNIQUE,
    password TEXT NOT NULL,
    PRIMARY KEY(id)
  ");

  create_table("account_sessions", "
    id CHAR(64) NOT NULL,
    date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    date_accessed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    account_id INT UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(account_id) REFERENCES accounts(id)
  ");

  create_table("account_roles", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    date_created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    date_deactived DATETIME,
    is_active TINYINT NOT NULL DEFAULT 1,
    attribute VARCHAR(191) NOT NULL,
    account_id INT UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(account_id) REFERENCES accounts(id)
  ");

  create_table("tags", "
    name VARCHAR(191),
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY(name)
  ");

  create_table("crons", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    months VARCHAR(191) NOT NULL DEFAULT '*',
    dates VARCHAR(191) NOT NULL DEFAULT '*',
    days VARCHAR(191) NOT NULL DEFAULT '*',
    hours VARCHAR(191) NOT NULL DEFAULT '*',
    minutes VARCHAR(191) NOT NULL DEFAULT '*',
    seconds VARCHAR(191) NOT NULL DEFAULT '*',
    PRIMARY KEY(id)
  ");

  create_table("computed_crons", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cron_id INT UNSIGNED NOT NULL,
    time DATETIME NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(cron_id) REFERENCES crons(id)
  ");

  create_table("times", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  create_table("time_tags", "
    time_id INT UNSIGNED NOT NULL,
    tag_name VARCHAR(191) NOT NULL,
    PRIMARY KEY(time_id, tag_name),
    FOREIGN KEY(time_id) REFERENCES times(id),
    FOREIGN KEY(tag_name) REFERENCES tags(name)
  ");

  create_table("time_crons", "
    cron_id INT UNSIGNED NOT NULL,
    time_id INT UNSIGNED NOT NULL,
    PRIMARY KEY(cron_id),
    FOREIGN KEY(cron_id) REFERENCES crons(id),
    FOREIGN KEY(time_id) REFERENCES times(id)
  ");

  create_table("audios", "
    md5 CHAR(32),
    filename VARCHAR(191) NOT NULL UNIQUE,
    filetype VARCHAR(191) NOT NULL,
    duration INT UNSIGNED NOT NULL DEFAULT 0,
    PRIMARY KEY(md5)
  ");

  create_table("playlists", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  create_table("playlist_audios", "
    playlist_id INT UNSIGNED,
    audio_md5 CHAR(32),
    position INT UNSIGNED,
    PRIMARY KEY(playlist_id, audio_md5),
    FOREIGN KEY(playlist_id) REFERENCES playlists(id),
    FOREIGN KEY(audio_md5) REFERENCES audios(md5)
  ");

  create_table("schedules", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(191) NOT NULL,
    PRIMARY KEY(id)
  ");

  create_table("schedule_tags", "
    schedule_id INT UNSIGNED NOT NULL,
    tag_name VARCHAR(191) NOT NULL,
    PRIMARY KEY(schedule_id, tag_name),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id),
    FOREIGN KEY(tag_name) REFERENCES tags(name)
  ");

  create_table("schedule_audios", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    audio_md5 CHAR(32),
    playlist_id INT UNSIGNED,
    position INT UNSIGNED,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id),
    FOREIGN KEY(audio_md5) REFERENCES audios(md5),
    FOREIGN KEY(playlist_id) REFERENCES playlists(id)
  ");

  create_table("schedule_include_times", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    cron_id INT UNSIGNED,
    time_id INT UNSIGNED,
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id),
    FOREIGN KEY(cron_id) REFERENCES crons(id),
    FOREIGN KEY(time_id) REFERENCES times(id)
  ");

  create_table("schedule_intersect_crons", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    months VARCHAR(191) NOT NULL DEFAULT '*',
    dates VARCHAR(191) NOT NULL DEFAULT '*',
    days VARCHAR(191) NOT NULL DEFAULT '*',
    hours VARCHAR(191) NOT NULL DEFAULT '*',
    minutes VARCHAR(191) NOT NULL DEFAULT '*',
    seconds VARCHAR(191) NOT NULL DEFAULT '*',
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id)
  ");

  create_table("schedule_exclude_crons", "
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schedule_id INT UNSIGNED NOT NULL,
    months VARCHAR(191) NOT NULL DEFAULT '*',
    dates VARCHAR(191) NOT NULL DEFAULT '*',
    days VARCHAR(191) NOT NULL DEFAULT '*',
    hours VARCHAR(191) NOT NULL DEFAULT '*',
    minutes VARCHAR(191) NOT NULL DEFAULT '*',
    seconds VARCHAR(191) NOT NULL DEFAULT '*',
    PRIMARY KEY(id),
    FOREIGN KEY(schedule_id) REFERENCES schedules(id)
  ");

  $hashed_password = password_hash("admin", PASSWORD_DEFAULT);

  execute_sql("INSERT INTO accounts (password, username) VALUES ('{$hashed_password}', 'admin')");

  execute_sql("INSERT INTO account_roles (account_id, attribute) VALUES (1, 'admin')");

  execute_sql("INSERT INTO times (name) VALUES ('Subuh'), ('Dzuhur'), ('Ashar'), ('Maghrib'), ('Isya')");

});
