<?php

require_once __DIR__ . "/app.php";

$data = require_json_data();

$result = execute_delete_sql("schedule_include_times", [
  "schedule_id" => [$data["schedule_id"], PDO::PARAM_INT],
  "time_id" => [$data["time_id"], PDO::PARAM_INT],
]);

send_json(200, $result);
