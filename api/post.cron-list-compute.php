<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/cron.php";

use_sql_transaction(function () {
  $crons = execute_sql("SELECT * FROM crons")->fetchAll();
  foreach ($crons as $cron) {
    execute_delete_sql("computed_crons", [
      "cron_id" => [$cron["id"], PDO::PARAM_INT],
    ]);
    $computed_crons = compute_cron($cron);
    foreach ($computed_crons as $computed_cron) {
      execute_insert_sql("computed_crons", $computed_cron);
    }
  }
});

send_json(200, ["ok" => true]);
