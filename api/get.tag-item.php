<?php

require_once __DIR__ . "/app.php";

$name = require_querystring("name");

$result = execute_sql("
  SELECT *
  FROM tags
  WHERE name = :name
", [
  ":name" => [$name, PDO::PARAM_STR],
])->fetch();

send_json(200, $result);
