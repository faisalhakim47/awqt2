<?php

require_once __DIR__ . "/../app.php";

function logout($token)
{
  execute_sql("
    DELETE FROM account_sessions WHERE id = :token
  ", [
    ":token" => $token,
  ]);
}
