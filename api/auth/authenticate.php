<?php

require_once __DIR__ . "/../app.php";

function __get_accountid_from_userattemp($user_attemp)
{
  $account = execute_sql("
    SELECT id
    FROM accounts
    WHERE id = :id
  ", [
    ":id" => [$user_attemp, PDO::PARAM_INT],
  ])->fetch();

  if (!$account) {
    $account = execute_sql("
      SELECT id
      FROM accounts
      WHERE username = ?
    ", [$user_attemp])->fetch();
  }

  if (!$account) {
    $account = execute_sql("
      SELECT id
      FROM accounts
      WHERE email = ?
    ", [$user_attemp])->fetch();
  }

  if ($account) {
    return (int) $account["id"];
  } else {
    return (int) $user_attemp;
  }
}

function login($user_attemp, $pass_attemp)
{
  $account_id = __get_accountid_from_userattemp($user_attemp);

  $hash = execute_sql("
    SELECT password
    FROM accounts
    WHERE id = :id
  ", [
    ":id" => [$account_id, PDO::PARAM_INT],
  ])->fetch()["password"];

  $is_pass_verified = password_verify($pass_attemp, $hash);

  if (!$is_pass_verified) {
    send_json(401, ["ok" => false]);
  }

  $token = password_hash(random_string(64), PASSWORD_DEFAULT);

  execute_sql("
    INSERT INTO account_sessions (id, account_id) VALUES (?, ?);
  ", [$token, $account_id]);

  return $token;
}
