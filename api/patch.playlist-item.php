<?php

require_once __DIR__ . "/app.php";

$playlist = require_json_data();

$result = execute_update_sql("playlists", [
  "name" => [$playlist["name"], PDO::PARAM_STR],
  "audio_md5s" => [$playlist["audio_md5s"], PDO::PARAM_STR],
], [
  "id" => [$playlist["id"], PDO::PARAM_INT],
]);

send_json(200, $result);
