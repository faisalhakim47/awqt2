<?php

require_once __DIR__ . "/app.php";

$data = require_json_data();

foreach ($data as $name => $value) {
  $config = execute_sql("
    SELECT name, value
    FROM configurations
    WHERE name = :name
  ", [
    ":name" => [$name, PDO::PARAM_STR],
  ])->fetch();
  if ($config) {
    if ($config["value"] !== $value) {
      execute_update_sql("configurations", [
        "value" => [$value, PDO::PARAM_STR],
      ], [
        "name" => [$name, PDO::PARAM_STR],
      ]);
    }
  } else {
    execute_insert_sql("configurations", [
      "name" => [$name, PDO::PARAM_STR],
      "value" => [$value, PDO::PARAM_STR],
    ]);
  }
}

send_json(200, ["ok" => true]);
