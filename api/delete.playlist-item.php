<?php

require_once __DIR__ . "/app.php";

$id = require_querystring("id");

$result = execute_delete_sql("playlists", [
  "id" => [$id, PDO::PARAM_INT],
]);

send_json(200, $result);
