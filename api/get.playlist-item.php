<?php

require_once __DIR__ . "/app.php";

$id = (int) require_querystring("id");

$result = execute_sql("
  SELECT id, name, audio_md5s
  FROM playlists
  WHERE playlists.id = :id
", [
  ":id" => [$id, PDO::PARAM_INT],
])->fetch();

send_json(200, $result);
