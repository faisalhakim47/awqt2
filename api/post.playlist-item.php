<?php

require_once __DIR__ . "/app.php";

$playlist = require_json_data();

$playlist_id = execute_insert_sql("playlists", [
  "name" => [$playlist["name"], PDO::PARAM_STR],
]);

send_json(200, [
  "id" => $playlist_id,
]);
