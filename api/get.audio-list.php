<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "filename" => [
    "sql_query" => "filename LIKE :filename",
    "param_type" => PDO::PARAM_STR,
  ],
]);

if ($_GET["get"] === 'size') {
  $data = $result = execute_sql("
    SELECT COUNT(*) AS size
    FROM audios
    {$prepare["sql_query"]}
  ", $prepare["params"])->fetch();
  send_json(200, $data);
}
else if ($_GET["get"] === 'data') {
  $limit = isset($_GET["limit"]) ? $_GET["limit"] : 184467440737095516;
  $offset = isset($_GET["skip"]) ? $_GET["skip"] : 0;
  
  $result = execute_sql("
    SELECT
      audios.md5 AS id,
      audios.md5 AS md5,
      audios.filename AS filename,
      audios.filetype AS filetype,
      audios.duration AS duration
    FROM audios
    {$prepare["sql_query"]}
    ORDER BY audios.filename ASC
    LIMIT :limit OFFSET :offset
  ", array_merge(
    [
      ":limit" => [(int) $limit, PDO::PARAM_INT],
      ":offset" => [(int) $offset, PDO::PARAM_INT],
    ],
    $prepare["params"]
  ))->fetchAll();

  send_json(200, array_map(function ($audio) {
    return array_merge($audio, [
      "id" => $audio["md5"],
    ]);
  }, $result));
}
else if (isset($_GET["md5s"])) {
  $md5s = implode("','", explode(",", $_GET["md5s"])) ;
  $result = execute_sql("
    SELECT
      audios.md5 AS id,
      audios.md5 AS md5,
      audios.filename AS filename,
      audios.filetype AS filetype,
      audios.duration AS duration
    FROM audios
    WHERE md5 IN ('{$md5s}')
  ")->fetchAll();
  send_json(200, array_map(function ($audio) {
    return array_merge($audio, [
      "id" => $audio["md5"],
    ]);
  }, $result));
}
else {
  send_json(400, [
    "msg" => "parameter does not setisfied",
  ]);
}
