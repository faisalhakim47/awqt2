<?php

require_once __DIR__ . "/app.php";

$time_cron = require_json_data();

execute_insert_sql("time_crons", [
  "cron_id" => [$time_cron["cron_id"], PDO::PARAM_INT],
  "time_id" => [$time_cron["time_id"], PDO::PARAM_INT],
]);

send_json(200, $time_cron);
