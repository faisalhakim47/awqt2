<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "keyword" => [
    "sql_query" => "name LIKE :keyword",
    "param_type" => PDO::PARAM_STR,
  ],
  "is_active" => [
    "sql_query" => "is_active = :is_active",
    "param_type" => PDO::PARAM_INT,
  ],
]);

$result = execute_sql("
  SELECT *
  FROM tags
  {$prepare["sql_query"]}
", $prepare["params"])->fetchAll();

send_json(200, $result);
