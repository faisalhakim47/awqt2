<?php

// MAIN
$config = json_decode(file_get_contents(__DIR__ . "/../../config.json"), true);

define("DEVELOPMENT", $config["DEVELOPMENT_MODE"]);
define("DB_USER", $config["DB_USER"]);
define("DB_PASS", $config["DB_PASS"]);
define("DB_HOST", $config["DB_HOST"]);
define("DB_PORT", $config["DB_PORT"]);
define("DB_NAME", $config["DB_NAME"]);
define("APP_URL", $config["APP_URL"]);
define("AUDIO_PATH", __DIR__ . "/../.." . $config["AUDIO_PATH"]);

$REFERER_WHITELIST = APP_URL;

if (DEVELOPMENT === true) {
  error_reporting(E_ALL ^ E_STRICT);
  error_reporting(error_reporting() & ~E_NOTICE);
  ini_set("display_errors", 1);
}

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: *");

if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
  die();
}

// if ($_SERVER["REQUEST_METHOD"] !== "POST") {
//   die();
// }

// TOOLS
require_once __DIR__ . "/tools/csrf.php";
require_once __DIR__ . "/tools/database.php";
require_once __DIR__ . "/tools/date.php";
require_once __DIR__ . "/tools/debug.php";
require_once __DIR__ . "/tools/email.php";
require_once __DIR__ . "/tools/enum.php";
require_once __DIR__ . "/tools/file.php";
require_once __DIR__ . "/tools/indonesia.php";
require_once __DIR__ . "/tools/scraper.php";
require_once __DIR__ . "/tools/server.php";
require_once __DIR__ . "/tools/string.php";
require_once __DIR__ . "/tools/upgrade.php";

// AUTH
require_once __DIR__ . "/auth/authenticate.php";
require_once __DIR__ . "/auth/authentication.php";
require_once __DIR__ . "/auth/authorization.php";
require_once __DIR__ . "/auth/unauthenticate.php";

// STRUCTURES
require_once __DIR__ . "/database/1.php";
require_once __DIR__ . "/database/2.php";
require_once __DIR__ . "/database/3.php";
require_once __DIR__ . "/database/4.php";

ensure_upgrade();
