<?php

require_once __DIR__ . "/app.php";

$data = require_json_data();

$cred = $data["ssid"] . ":" . $data["passphrase"];

$result = json_decode(file_get_contents("http://127.0.0.1:3131/set_access_point/" . $cred), true);

send_json(200, $result);
