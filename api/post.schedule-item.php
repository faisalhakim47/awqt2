<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/schedule.php";

$schedule = require_json_data();

$result = create_schedule_pack($schedule);

send_json(200, $result);
