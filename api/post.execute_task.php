<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/awqt.php";

$task_name = require_querystring("name");
$payload = get_querystring("payload");

$result = execute_task($task_name, $payload);

send_json(200, [
  "ok" => true,
  "result" => $result,
]);
