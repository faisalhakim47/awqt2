#!/usr/bin/python

import RPi.GPIO as GPIO
from http.server import BaseHTTPRequestHandler, HTTPServer

GPIO.setmode(GPIO.BCM)

PINS = []


def ensure_pin(pin):
    if pin in PINS:
        return
    else:
        GPIO.setup(pin, GPIO.IN)
        GPIO.setup(pin, GPIO.OUT)
        PINS.append(pin)


def get_status(pin):
    ensure_pin(pin)
    return GPIO.input(pin)


def turn_on(pin):
    ensure_pin(pin)
    return GPIO.output(pin, GPIO.LOW)


def turn_off(pin):
    ensure_pin(pin)
    return GPIO.output(pin, GPIO.HIGH)


def toggle_relay(pin):
    if get_status(pin) == 1:
        return turn_on(pin)
    else:
        return turn_off(pin)


class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        partial_path = self.path.split('/')
        if partial_path[1] != 'pin':
            self.wfile.write(bytes('{ "ok": true }', 'utf8'))
            return
        pin = int(partial_path[2])
        mode = partial_path[3]
        if mode == 'on':
            turn_on(pin)
            self.wfile.write(bytes('{ "ok": true }', 'utf8'))
        elif mode == 'off':
            turn_off(pin)
            self.wfile.write(bytes('{ "ok": true }', 'utf8'))
        elif mode == 'toggle':
            toggle_relay(pin)
            self.wfile.write(bytes('{ "ok": true }', 'utf8'))
        elif mode == 'status':
            status = get_status(pin) == 0
            is_active = 'true' if status else 'false'
            self.wfile.write(
                bytes('{ "ok": true, "isActive": ' + is_active + ' }', 'utf8'))
        else:
            self.wfile.write(bytes('{ "ok": true }', 'utf8'))


print('SERVER STARTED ON 127.0.0.1:9000')

SERVER_ADDRESS = ('127.0.0.1', 9000)
HTTPD = HTTPServer(SERVER_ADDRESS, testHTTPServer_RequestHandler)
HTTPD.serve_forever()
